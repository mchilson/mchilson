### Welcome! :v:
#### Hi, and welcome to my Gitlab repository. I am a freelance developer with 37 years experience living in Birmingham, Alabama, USA :statue_of_liberty:. I specialize in Ruby, PHP, R, C, FORTRAN, and PROLOG. Most of the public code here on GitLab is my personal experimentation with various languages, algorithms, tools, and a few small open-source projects. :blush:
 
**Skills** :hammer: HTML / CSS / JS / RUBY / PHP / R LANG / C / FORTRAN / PROLOG / SQL
  
**You can learn more about me by visting my website: https://mikechilson.com**
